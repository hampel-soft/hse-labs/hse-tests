# HSE Tests

Static and dynamic code analysis

## VI Analyzer

The latest documentation can be found at 
https://dokuwiki.hampel-soft.com/kb/labview/tools/vianalyzer.



### :rocket: Installation

See https://dokuwiki.hampel-soft.com/kb/labview/tools/vianalyzer


### Subdiagram Labels

* Checks for the visibility of subdiagram labels
* Looks inside case, disable and event structures
* optionally checks for labels to be not empty
* optionally skips error structures

### Block diagram background colours

* Checks the background colour of block diagrams
* Accepts two grey hues as upper/lower limits

### Special Chars in Documentation

*Written by Benjamin Hinrichs*

* Looks for extended ASCII characters (https://en.wikipedia.org/wiki/Extended_ASCII) in the VI description, as these can cause problems with programmatically processing the description outside LabVIEW (eg generating documentation).


## :wrench: LabVIEW 2014

The VIs are maintained in LabVIEW 2014.


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Joerg Hampel
* Manuel Sebald
* Benjamin Hinrichs

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
